#!/bin/sh

if (! grep -i vf.com /etc/resolv.conf > /dev/null);
then
    sed -i "1 i\search vf.com" /etc/resolv.conf

    if (! grep -i "192.168.62.5" /etc/resolv.conf);
    then
        sed -i "/vf.com/a nameserver 192.168.62.5" /etc/resolv.conf
    fi
elif (grep -i vf.com /etc/resolv.conf > /dev/null);
then
    if (! grep -i "192.168.62.5" /etc/resolv.conf);
    then
        sed -i "/vf.com/a nameserver 192.168.62.5" /etc/resolv.conf
    fi
fi

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDqYsOA6BqcFZw75fKoU/SdMtXYLwJv+UiakX2aRmqipncCJ+jmh6c5CAePpjKJHLBH7+xCCm2cklH3WugPanAd6n8CxcDEPtfgWr7RvW5djYkYYmuic2CZw4hCgk24olK7ycCE4nCl5j1wKtXmyd62obObmsMGNEwYebhCu7Qv88TGMw66k6jfR54bDhsFHwTIY3bivq0CgP9O5fnx8f2Su165YmOLmi764FnpPreOjegTFPPGNSE3nCqd0qfVTJZaQ8EyUT1rg2GSFQRY9z5hB+e8Z9xw6/HLsxiZ65DUHBtVYqCR4x1CLEqUZkBp8plGD+AfHN4AJnDBMwWUYB6l root@pulpo.viajesfalabella.com" >> ~/.ssh/authorized_keys

